<?php
include_once('../../../vendor/autoload.php');
use \App\BITM\SEIP106611\AddressBook\AddressBook;
use \App\BITM\SEIP106611\Utility\Utility;

    $addressBook = new AddressBook();
    $phones = $addressBook->trashed();
    
    
    
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Trrashed </title>
	<link rel="stylesheet" href="../../../Resource/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../../Resource/css/style.css">
	
	<style type="text/css">

	.text_align{
		text-align:center;
	}
	</style>
	
  </head>
  <body>
		<div class="wrapper">
			<div class="header">
				<h4>Phone Book</h4>
			</div>
								<div class="main_content">
								
									<div class="form-horizontal">
										<div class="form-group">
											<div class="row  col-md-16">
									
												<div class="  col-md-2">
													<form class="ajax" action="#" method="post">
														<select class="items" name="items">
															<option value="15">10</option>
															<option value="20">20</option>
															<option value="30">30</option>
															<option value="40">40</option>
														</select>
													</form>
												</div>
										
												<div class=" col-md-2">
													<a  class="btn btn-success btn-xs" href="#">SEARCH</a>
												</div>
										
												<div class=" col-md-8">
																							
													<div class="row">
														<div class=" col-md-4">
																		
															<p class="textRight">Download</p>
														</div>
														<div class="col-md-8">
																			
															<a class="btn btn-success btn-xs" href="#">PDF</a>
																				
															<a class="btn btn-success btn-xs" href="#">EXCEL</a>
															<a class="btn btn-warning btn-xs" href="index.php" class="list-btn">Contract</a>
																				
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
							
									<div class="row main_info">
										<div class="col-md-12">
											<div class="box-table">	
												<table class="table table-border table-hover">
													<thead class="text-center">
														<tr>
															<th  class="text_align">Serial</th>
															<th  class="text_align">Photo</th>
															<th  class="text_align">Name</th>
															<th  class="text_align">Number</th>
															<th  class="text_align">Action</th>
															</tr>
													</thead>
													<tbody class="text-center">
														<?php
														   $count =1;
														   foreach($phones as $phone){
														?>
														<tr>
															<td><?php echo $count;?></td>
															<td><a href="show.php?id=<?php echo $phone->id;?>"><?php echo $phone->photo;?></a></td>
															<td><?php echo $phone->first_name." ".$phone->last_name;?></td>
															<td><?php echo $phone->mobile_phone;?></td>
														
															<td>
																<a class="btn btn-danger btn-xs" href="delete.php?id=<?php echo $phone->id;?>" class="list-btn">Delete</a>
																
																<a class="btn btn-warning btn-xs" href="recover.php?id=<?php echo $phone->id;?>" class="list-btn">recover</a>
																
															</td>
														</tr>
														<?php
															$count++;
														}
														?>
													
													</tbody>
												</table>
											</div>
					
										</div> 
					
									</div>
								</div>
		</div>
      <p class="text-center"><a href="index.php">Go to Homepage</a></p>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
   <script src="../../../../Resource/bootstrap/js/bootstrap.min.js"></script>
    <script>
           $('.delete').bind('click',function(e){
               var deleteItem = confirm("Are you sure you want to delete?");
               if(!deleteItem){
                  //return false; 
                  e.preventDefault();
               }
           }); 
    </script>
  </body>
</html>